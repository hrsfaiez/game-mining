package kmeans.clustering.mapreduce;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static kmeans.clustering.mapreduce.CombinerAsReducer.from;

public class CombineTest {
    Combine combine;
    ReduceDriver<Text, Text, Text, Text> drive;

    @Before
    public void setup() {
        combine = new Combine();
        drive   = ReduceDriver.newReduceDriver(from(combine));
    }

    @Test
    public void pointsForSingleCentroid() throws Exception {
        Text expectedOutput = new Text("8.0,8.0,8.0,8.0,8.0,8.0,8.0,8.0,8.0,8.0~3");

        drive.withInput(new Text("centroid0"), mapOutput())
             .withOutput(new Text("centroid0"), expectedOutput)
             .runTest();
    }

    private List<Text> mapOutput() {
        List<Text> result = new LinkedList<>();
        result.add(new Text("dp0\t0,0,0,0,0,0,0,0,0,0"));
        result.add(new Text("dp5\t5,5,5,5,5,5,5,5,5,5"));
        result.add(new Text("dp3\t3,3,3,3,3,3,3,3,3,3"));
        return result;
    }
}
