package kmeans.clustering.mapreduce;

import kmeans.clustering.mapreduce.fake.FakeGameMining;
import kmeans.clustering.mapreduce.spy.ReduceSpy;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SingleReduceTest {

    FakeGameMining configuration;
    ReduceDriver<Text, Text, Text, Text> driver;

    @Before
    public void setUp() {
        configuration = FakeGameMining.withEdgeCentroids();
        driver = ReduceDriver.newReduceDriver(new ReduceSpy(configuration));
    }

    @Test
    public void variedSingleCentroid() throws IOException {
        Text actualInput = new Text("5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,0.0~5");
        Text expectedOutput = new Text("1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,0.0");

        driver.withInput(new Text("centroid0"),  singletonList(actualInput))
              .withOutput(new Text("centroid0"), expectedOutput)
              .runTest();

        assertEquals(configuration.anotherFixedCentroidCalls, Integer.valueOf(0));
        assertFalse(configuration.isOver());
        assertEquals(expectedOutput.toString(), configuration.centroids().get("centroid0"));
    }

    @Test
    public void unvariedSingleCentroid() throws IOException {
        Text nullInput = new Text("0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0~3");
        Text expectedOutput = new Text("0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0");

        driver.withInput(new Text("centroid0"),  singletonList(nullInput))
              .withOutput(new Text("centroid0"), expectedOutput)
              .runTest();

       assertEquals(configuration.anotherFixedCentroidCalls, Integer.valueOf(1));
    }

}
