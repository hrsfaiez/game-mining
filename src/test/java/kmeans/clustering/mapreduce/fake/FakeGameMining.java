package kmeans.clustering.mapreduce.fake;

import kmeans.entry.GameMining;

import java.util.HashMap;

public class FakeGameMining extends GameMining {
    public Integer anotherFixedCentroidCalls = 0;

    private FakeGameMining(HashMap<String, String> centroids) {
        super(centroids);
    }

    @Override
    public void fixedCentroid() {
        super.fixedCentroid();
        anotherFixedCentroidCalls ++;
    }

    public static FakeGameMining withEdgeCentroids() {
        final HashMap<String, String> centroids = new HashMap<>();
        centroids.put("centroid0", "0,0,0,0,0,0,0,0,0,0");
        centroids.put("centroid100", "100,100,100,100,100,100,100,100,100,100");
        return new FakeGameMining(centroids);
    }
}
