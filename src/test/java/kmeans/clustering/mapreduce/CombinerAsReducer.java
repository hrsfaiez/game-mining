package kmeans.clustering.mapreduce;


import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class CombinerAsReducer extends Reducer<Text, Text, Text, Text> {
    private Combine combine;

    private CombinerAsReducer(Combine combine) {
        this.combine = combine;
    }

    public static Reducer<Text, Text, Text, Text> from(Combine combine) {
        return new CombinerAsReducer(combine);
    }

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        combine.reduce(key, values, context);
    }
}
