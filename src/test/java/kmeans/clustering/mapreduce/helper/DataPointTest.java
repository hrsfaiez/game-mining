package kmeans.clustering.mapreduce.helper;


import kmeans.clustering.mapreduce.helper.DataPoint;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DataPointTest {

    @Test
    public void effectiveInput() {
        String inputLine
                = "d0\t8776,396911,227521,815402,647540,915734,289109,118412,367985,839209";
        DataPoint arbitrary = DataPoint.at(inputLine);
        List expectedCoordinates
                = Arrays.asList(8776, 396911, 227521, 815402, 647540,
                                915734, 289109, 118412, 367985, 839209);
        assertEquals(expectedCoordinates, arbitrary.coordinates());
    }

    @Test
    public void fourthLocation() {
        String arbitraryLine = "d0\t11,12.1,14,16";
        DataPoint arbitrary = DataPoint.at(arbitraryLine);
        List expectedCoordinates = Arrays.asList(11, 12.1, 14, 16);
        assertEquals(expectedCoordinates, arbitrary.coordinates());
    }

    @Test
    public void tripleLocation() {
        String arbitraryLine = "d0\t11,12,14.345";
        DataPoint arbitrary = DataPoint.at(arbitraryLine);
        List expectedCoordinates = Arrays.asList(11, 12, 14.345);
        assertEquals(expectedCoordinates, arbitrary.coordinates());
    }

    @Test
    public void doubleLocation() {
        String arbitraryLine = "d0\t11,12";
        DataPoint arbitrary = DataPoint.at(arbitraryLine);
        List expectedCoordinates = Arrays.asList(11, 12);
        assertEquals(expectedCoordinates, arbitrary.coordinates());
    }

    @Test
    public void singletonLocation() {
        String arbitraryLine = "d0\t11";
        DataPoint arbitrary = DataPoint.at(arbitraryLine);
        List expectedCoordinates = Collections.singletonList(11);
        assertEquals(expectedCoordinates, arbitrary.coordinates());
    }

    @Test
    public void identity() {
        String initialLine = "d0\t0";
        DataPoint initial = DataPoint.at(initialLine);
        assertEquals("d0", initial.id());
    }
}
