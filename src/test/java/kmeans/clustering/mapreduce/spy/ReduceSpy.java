package kmeans.clustering.mapreduce.spy;

import kmeans.clustering.mapreduce.fake.FakeGameMining;
import kmeans.clustering.mapreduce.Reduce;
import kmeans.entry.GameMining;

public class ReduceSpy extends Reduce {
    private FakeGameMining configuration;

    public ReduceSpy(FakeGameMining configuration) {
        super();
        this.configuration = configuration;
    }

    @Override
    protected GameMining getConfiguration() {
        return configuration;
    }
}
