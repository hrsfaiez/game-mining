package kmeans.clustering.mapreduce.spy;

import kmeans.clustering.mapreduce.Map;
import kmeans.entry.GameMining;

public class MapSpy extends Map {
    GameMining configuration;
    public MapSpy(GameMining aConfiguration) {
        configuration = aConfiguration;
    }

    @Override
    protected GameMining getConfiguration() {
        return configuration;
    }
}
