package kmeans.clustering.mapreduce;

import kmeans.clustering.mapreduce.fake.FakeGameMining;
import kmeans.clustering.mapreduce.spy.MapSpy;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

public class MultipleDataPointsMapTest {
    public final LongWritable someId  = new LongWritable(0L);
    public final LongWritable otherId = new LongWritable(1L);

    MapDriver<LongWritable, Text, Text, Text> drive;

    @Before
    public void setUp() {
        Map map = new MapSpy(FakeGameMining.withEdgeCentroids());
        drive   = MapDriver.newMapDriver(map);
    }

    @Test
    public void multipleDataPoints_EachNearACentroid() throws Exception {
        Text pointNearCentroid100 = new Text("dp90\t90,90,90,90,90,90,90,90,90,90");
        Text pointNearCentroid0   = new Text("dp0\t0,0,0,0,0,0,0,0,0,0");

        drive.withInput(someId,  pointNearCentroid100)
             .withInput(otherId, pointNearCentroid0)
             .withOutput(new Text("centroid100"), pointNearCentroid100)
             .withOutput(new Text("centroid0"),   pointNearCentroid0)
             .runTest();
    }

}
