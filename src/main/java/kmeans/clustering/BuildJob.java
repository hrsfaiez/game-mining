package kmeans.clustering;

import kmeans.clustering.mapreduce.Combine;
import kmeans.clustering.mapreduce.Map;
import kmeans.clustering.mapreduce.Reduce;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class BuildJob {
    public String input;
    public String output;
    private Class container;

    public BuildJob(Class container, String input, String output) throws IOException {
        this.container = container;
        this.input = input;
        this.output = output;
    }

    public Job get() throws Exception {
        Configuration conf = new Configuration();
        Job jobConfiguration = Job.getInstance(conf, "word count");
        jobConfiguration.setJarByClass(container);

        jobConfiguration.setMapperClass(Map.class);
        jobConfiguration.setReducerClass(Reduce.class);
        jobConfiguration.setCombinerClass(Combine.class);

        jobConfiguration.setMapOutputKeyClass(Text.class);
        jobConfiguration.setMapOutputValueClass(Text.class);

        jobConfiguration.setOutputKeyClass(Text.class);
        jobConfiguration.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(jobConfiguration, new Path(input));
        FileOutputFormat.setOutputPath(jobConfiguration, new Path(output));

        return jobConfiguration;
    }

    public static BuildJob with(Class container, String input, String output)
            throws IOException {
        return new BuildJob(container, input, output);
    }
}
