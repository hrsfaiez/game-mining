package kmeans.clustering.mapreduce;

import kmeans.clustering.mapreduce.helper.DataPoint;
import kmeans.entry.GameMining;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.HashMap;

import static kmeans.entry.GameMining.get;

public class Map extends Mapper<LongWritable, Text, Text, Text> {
    private DataPoint current;

    @Override
    protected void map(LongWritable eachId,
                       Text         eachDataPoint,
                       Context      output)
            throws IOException, InterruptedException {

        output.write(idOfNearestTo(eachDataPoint, actualCentroids()), eachDataPoint);
    }

    private Text idOfNearestTo(Text current,
                               HashMap<String, String> centroids)
            throws IOException {
        this.current = DataPoint.fromInput(current);
        return new Text(idOfNearestIn(centroids));
    }

    private String idOfNearestIn(HashMap<String, String> dataPoints)
            throws IOException {
        String result = "";
        Double shortest = Double.MAX_VALUE;
        for (String eachId : dataPoints.keySet()) {
            DataPoint eachCentroid = DataPoint.fromInput(eachId + "\t" + dataPoints.get(eachId));
            Double eachDistance = DataPoint.distanceBetween(eachCentroid, current);
            if (eachDistance < shortest) {
                shortest = eachDistance;
                result = eachId;
            }
        }
        return result;
    }

    private HashMap<String, String> actualCentroids() {
        return getConfiguration().centroids();
    }

    protected GameMining getConfiguration() {
        return get();
    }
}
