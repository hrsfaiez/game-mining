package kmeans.clustering.mapreduce.helper;

import org.apache.hadoop.io.Text;

import java.util.List;

public class DataPoint {

    private String theValue;
    private Double[] theParts;
    private List coordinates;

    public DataPoint(String aValue, Double[] aParts) {
        this.theValue = aValue;
        this.theParts = aParts;
    }

    public static DataPoint fromInput(String aValue) {
        String aDataValue = aValue.split("\t")[1];
        String[] s = aDataValue.split(",");
        Double[] result = new Double[s.length];
        for (int i = 0; i < s.length; i++) {
            result[i] = Double.parseDouble(s[i]);
        }
        return new DataPoint(aValue, result);
    }

    private static Double distanceBetween(Double[] aCentroid, Double[] aDataPoint) {
        return Math.sqrt(
                Math.pow(aCentroid[0] - aDataPoint[0], 2) + Math.pow((aCentroid[1] - aDataPoint[1]), 2) +
                        Math.pow(aCentroid[2] - aDataPoint[2], 2) + Math.pow(aCentroid[3] - aDataPoint[3], 2) +
                        Math.pow(aCentroid[4] - aDataPoint[4], 2) + Math.pow(aCentroid[5] - aDataPoint[5], 2) +
                        Math.pow(aCentroid[6] - aDataPoint[6], 2) + Math.pow(aCentroid[7] - aDataPoint[7], 2) +
                        Math.pow(aCentroid[8] - aDataPoint[8], 2) + Math.pow(aCentroid[9] - aDataPoint[9], 2));

    }

    public static Double distanceBetween(DataPoint aCentroid, DataPoint aDataPoint) {
       return distanceBetween(aCentroid.parts(), aDataPoint.parts());
    }

    public String value() {
        return theValue;
    }

    public Double[] parts() {
        return theParts;
    }


    public static DataPoint at(String entry) {
        Line line = Line.from(entry);
        return new DataPoint(line.id(), line.coordinates());
    }

    private String id;

    public DataPoint(String id, List coordinates) {
        this.coordinates = coordinates;
        this.id = id;
    }

    public String id() {
        return id;
    }

    public List coordinates() {
        return coordinates;
    }

    public Double coordinate(int index) {
        return parts()[index];
    }

    public static DataPoint fromInput(Text dataPoint) {
        return fromInput(dataPoint.toString());
    }
}
