package kmeans.clustering.mapreduce.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Line {
    public static final String SPACE = "\t";
    public static final String DECIMAL = ".";
    public static final String COMMA = ",";
    public static final List DELIMITERS = Arrays.asList(',', '\t');
    String value;

    private Line(String value) {
        this.value = value;
    }

    String id() {
        return value.substring(0, value.indexOf(SPACE));
    }

    ArrayList coordinates() {
        Integer threshold = value.indexOf(SPACE);
        ArrayList result = new ArrayList();
        for (Integer coordinateIndex = 0; coordinateIndex < 10; coordinateIndex++) {
            if (hasCoordinateAfter(threshold)) {
                Integer beginning = threshold + 1;
                threshold = endingNext(threshold);
                if (hasCoordinateAfter(threshold))
                    result.add(coordinateIn(beginning, threshold));
                else
                    result.add(coordinateIn(beginning, value.length()));
            }
        }
        return result;
    }

    private Boolean hasCoordinateAfter(Integer index) {
        return DELIMITERS.contains(value.charAt(index));
    }

    private Integer endingNext(Integer beginning) {
        String remaining = withoutStartingDelimiter(beginning);
        return beginning + endingNext(beginning, remaining);
    }

    private Integer endingNext(Integer beginning, String coordinates) {
        Integer offset = delimiterOffset(coordinatesAfter(beginning));
        if (isLast(coordinates))
            return lastEnding() - beginning;
        return offset + firstEndingIn(coordinates);
    }

    private String withoutStartingDelimiter(Integer beginning) {
        String result = coordinatesAfter(beginning);
        if (result.startsWith(COMMA))
            return result.substring(1, result.length());

        return result;
    }

    private Integer delimiterOffset(String coordinates) {
        Integer delimiterLength = 0;
        if (coordinates.startsWith(COMMA)) {
            delimiterLength = 1;
        }
        return delimiterLength;
    }

    private Integer lastEnding() {
        return value.length() - 1;
    }

    private Boolean isLast(String coordinates) {
        return delimiterIn(coordinates) == -1;
    }

    private Integer firstEndingIn(String coordinates) {
        return firstCoordinateIn(coordinates).length();
    }

    private String firstCoordinateIn(String coordinates) {
        return coordinates.substring(0, delimiterIn(coordinates));
    }

    private Integer delimiterIn(String coordinates) {
        return coordinates.indexOf(COMMA);
    }

    private String coordinatesAfter(Integer index) {
        return value.substring(index, value.length());
    }

    private Number coordinateIn(Integer start, Integer end) {
        String subject = coordinateStringIn(start, end);
        if (subject.contains(DECIMAL))
            return Double.parseDouble(subject);
        return Integer.parseInt(subject);
    }

    private String coordinateStringIn(Integer start, Integer end) {
        return value.substring(start, end);
    }

    public static Line from(String value) {
        return new Line(value);
    }
}
