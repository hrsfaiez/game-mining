package kmeans.clustering.mapreduce;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

import static kmeans.clustering.mapreduce.helper.DataPoint.fromInput;


public class Combine extends Reducer<Text, Text, Text, Text> {
    public static final String  SPLIT_SUM_FROM_COUNT = "~";
    public static final Integer COORDINATE_COUNT     = 10;
    public static final String  COORDINATE_DELIMITER = ",";

    private Double[] sum;
    private Integer counter;

    @Override
    protected void reduce(Text           centroidId,
                          Iterable<Text> dataPoints,
                          Context        output)
            throws IOException, InterruptedException {

        output.write(centroidId, sumAndCountOf(dataPoints));
    }

    private Text sumAndCountOf(Iterable<Text> dataPoints) {
        initializeCount();
        initializeSum();
        sumAndCount(dataPoints.iterator());
        return new Text(String.format("%s%s%s", sum(), SPLIT_SUM_FROM_COUNT, count()));
    }

    private String sum() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < COORDINATE_COUNT; i++) {
            result.append(sum[i]);
            if (thereIsCoordinateAfter(i))
                result.append(COORDINATE_DELIMITER);
        }
        return result.toString();
    }

    private boolean thereIsCoordinateAfter(Integer index) {
        return index < COORDINATE_COUNT - 1;
    }

    private String count() {
        return counter.toString();
    }

    private void sumAndCount(Iterator<Text> dataPoints) {
        while (dataPoints.hasNext()) {
            incrementCounter();
            sumPlus(dataPoints.next());
        }
    }

    private void sumPlus(Text dataPoint) {
        for (int i = 0; i < COORDINATE_COUNT; i++)
            sum[i] += fromInput(dataPoint.toString()).coordinate(i);
    }

    private void incrementCounter() {
        counter++;
    }

    private void initializeCount() {
        counter = 0;
    }

    private void initializeSum() {
        sum = new Double[COORDINATE_COUNT];
        for (int i = 0; i < COORDINATE_COUNT; i++)
            sum[i] = 0d;
    }
}
