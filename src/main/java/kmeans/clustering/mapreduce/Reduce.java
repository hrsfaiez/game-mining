package kmeans.clustering.mapreduce;

import kmeans.clustering.mapreduce.helper.DataPoint;
import kmeans.entry.GameMining;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import static kmeans.entry.GameMining.get;

public class Reduce extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(Text           oldCentroidId,
                          Iterable<Text> eachSumAndCount,
                          Context        output)
            throws IOException, InterruptedException {
        String newCentroid = averageOf(eachSumAndCount);

        if (centroidIsFixed(oldCentroidId, newCentroid))
            fixedCentroid();
        else
            updatedCentroid(oldCentroidId, newCentroid);

        output.write(oldCentroidId, outputOf(newCentroid));
    }

    private String averageOf(Iterable<Text> value) {
        return averageOf(value.iterator());
    }

    private void updatedCentroid(Text oldCentroidId, String newCentroid) {
        centroids().replace(oldCentroidId.toString(),
                            centroids().get(oldCentroidId.toString()),
                            newCentroid);
    }

    private Boolean centroidIsFixed(Text oldCentroidId, String newCentroid) {
        String oldCentroidCoordinates = centroids().get(oldCentroidId.toString());
        DataPoint oldCentroid = DataPoint.fromInput(oldCentroidId.toString() + "\t" + oldCentroidCoordinates);
        DataPoint result = DataPoint.fromInput(oldCentroid.toString() + "\t" + newCentroid);
        return DataPoint.distanceBetween(oldCentroid, result) < 0.001;
    }

    private String averageOf(Iterator<Text> value) {
        Double[] allSum = new Double[10];
        Integer counter = 0;
        String resultCentroid = "";

        for (int i = 0; i < 10; i++)
            allSum[i] = 0d;
        while (value.hasNext()) {
            String eachValue = value.next().toString();
            String[] eachValueParts = eachValue.split("~");

            String eachValueCounter = eachValueParts[1];
            counter += Integer.parseInt(eachValueCounter);

            DataPoint eachDataPoint = DataPoint.fromInput("new" + "\t" + eachValueParts[0]);

            Double[] eachDataPointDoubleParts = DataPoint.fromInput(eachDataPoint.value()).parts();

            for (int i = 0; i < 10; i++)
                allSum[i] += eachDataPointDoubleParts[i];

            StringBuilder reducedValue = new StringBuilder();
            for (int i = 0; i < 10; i++) {
                allSum[i] = allSum[i] / counter;
                reducedValue.append(String.valueOf(allSum[i])).append(",");
            }
            resultCentroid = reducedValue.toString().substring(0, reducedValue.toString().length() - 1);
        }
        return resultCentroid;
    }

    protected GameMining getConfiguration() {
        return get();
    }

    private HashMap<String, String> centroids() {
        return getConfiguration().centroids();
    }

    private void fixedCentroid() {
        getConfiguration().fixedCentroid();
    }

    private Text outputOf(String output) {
        return new Text(output);
    }
}
