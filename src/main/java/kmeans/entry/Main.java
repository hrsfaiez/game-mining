package kmeans.entry;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws Exception {
        String input         = args[0];
        String output        = args[1];
        GameMining.initialize(centroids());
        GameMining.get().launch(Main.class, input, output);
    }

    private static HashMap<String, String> centroids() {
        HashMap<String, String> centroids = new HashMap<>();
        centroids.put("C1", "23357,401753,229671,826166,670144,946988,255137,89322,361894,828360");
        centroids.put("C2", "167909,260564,428079,837156,182538,817263,439382,135960,263912,648169");
        centroids.put("C3", "46153,400715,236515,818781,671558,914860,271018,91979,376398,821506");
        centroids.put("C4", "53709,374748,246537,791761,657181,918538,276855,88861,357334,845216");
        centroids.put("C5", "53717,379826,219550,800236,654848,937875,266376,79485,363165,823282");
        return centroids;
    }
}
