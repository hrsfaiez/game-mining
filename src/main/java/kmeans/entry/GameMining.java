package kmeans.entry;

import kmeans.clustering.BuildJob;
import org.apache.hadoop.mapreduce.Job;

import java.util.HashMap;

public class GameMining {
    private HashMap<String, String> centroids;
    private Integer unvariedCentroidCount = 0;

    public GameMining(HashMap<String, String> centroids) {
        this.centroids = centroids;
    }

    protected void launch(Class container, String input, String outputBase)
            throws Exception {
        Integer eachIndex = 0;
        while (! isOver()) {
            unvariedCentroidCount = 0;
            String eachOutput = outputBase + (eachIndex + 1);
            Job eachJob = BuildJob.with(container, input, eachOutput).get();
            eachJob.waitForCompletion(true);
            eachIndex++;
        }
    }

    public Boolean isOver() {
        return unvariedCentroidCount.equals(centroids.size());
    }

    public void fixedCentroid() {
        unvariedCentroidCount = unvariedCentroidCount + 1;
    }

    public HashMap<String, String> centroids() {
        return centroids;
    }

    private static GameMining instance;
    public static GameMining get() {
        if (instance == null)
            throw new RuntimeException("GameMining cannot be null");
        return instance;
    }

    public static void initialize(HashMap<String, String> centroids) {
        instance = new GameMining(centroids);
    }
}
